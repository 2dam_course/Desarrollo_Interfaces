/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * FXML Controller class
 *
 * @author dirrospace
 */
public class SongDetailsController implements Initializable {

    @FXML
    private Label _name;
    @FXML
    private Label _artist;
    @FXML
    private Label _album;
    @FXML
    private ImageView _cover;
    @FXML
    private Label _genre;
    @FXML
    private Label _time;



    /**
     * El contructor se llama despues de la inicializacion
     */
    
    
    
    public SongDetailsController() {
    }    

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    


    @FXML
    private void onEdit(ActionEvent event) {
    }

    @FXML
    private void onDelete(ActionEvent event) {
    }



    public void setName(String _name) { this._name.setText(_name); }
    
    public void setArtist(String _artist) { this._artist.setText(_artist); }
    
    public void setAlbum(String _album) { this._album.setText(_album);  }
    
    public void setCover(Image _cover) {  this._cover.setImage(_cover); }

    public void setGenre(String _genre){ this._genre.setText(_genre); }
    
    public void setTime(LocalDate _localdate){ 
        if(_localdate!=null)this._time.setText(DateTimeFormatter.ofPattern("dd.MM.yyyy").format(_localdate));
        else this._time.setText("");
    }
    
//    public Label getAlbum() { return _album; }
//    public ImageView getCover() { return _cover; }
//    public Label getName() { return _name; }
//    public Label getArtist() { return _artist; }
//    public Label getGenre () { return _genre; }

    @FXML
    private void onAdd(ActionEvent event) {
        
    }
    
    
}
