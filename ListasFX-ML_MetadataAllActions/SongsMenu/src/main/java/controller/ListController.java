/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import di.desarrollo.menucanciones.MenuSongs;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import model.Lista;
import model.Song;

/**
 * FXML Controller class
 *
 * @author dirrospace
 */
public class ListController implements Initializable {
    private Desktop desktop = Desktop.getDesktop();
    // Usado para internar
    private MenuSongs mainmenu;
    public void setMainmenu(MenuSongs mainmenu){ this.mainmenu = mainmenu;  }
    
    @FXML
    private TableColumn<Song, String> _name;
    @FXML
    private TableColumn<Song, String> _type;
    @FXML
    private TableView<Song> _songs;
    // -- OPCION1 --
    @FXML
    private VBox songdetails;
    @FXML
    private SongDetailsController songdetailsController;
    // obligat whateveridincludefxml+controller
    
//    //OPCION2 >>> NO FUNCIONA
//    private SongDetailsController songdetailsController;
//    private VBox vb ;
   
    private Lista list = new Lista();
    /**
     * Initializes the controller class.
     */
    public ListController(){
//    //OPCION2 >>> NO FUNCIONA
//        try {
//            FXMLLoader fxml = new FXMLLoader(getClass().getResource("/fxml/songDetails.fxml"));
//            vb = (VBox) fxml.load();
//            songdetailsController = fxml.getController();
//        } catch (IOException ex) {
//            Logger.getLogger(ListController.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        _songs.setItems(list.getListaCancion());
        _name.setCellValueFactory(cellData -> cellData.getValue().getTitleProperty());
        _type.setCellValueFactory(cellData -> cellData.getValue().getTypeProperty());
        list.addToListSong(new File(getClass().getResource("/songs/anastasia.mp3").getPath()));
//        list.addToListSong(new File(getClass().getResource("/songs/tarzan.mp3").getPath()));
        showSongsDetails(null);
            
//            // Listen for selection changes and show the person details when changed.
        _songs.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showSongsDetails(newValue));
       
    }    




    
    // Link the ram sources with the local variables
    private void showSongsDetails(Song object) {
        if (object == null){
            Image im = new Image(getClass().getResourceAsStream("/images/cover_x.png"));
            songdetailsController.setCover(im);
            songdetailsController.setName(" ... ");
            songdetailsController.setArtist(" ... ");
            songdetailsController.setAlbum(" ... ");
            songdetailsController.setGenre(" ... ");
            songdetailsController.setTime(null);
        } else{
            songdetailsController.setCover(object.getCover());
            songdetailsController.setName(object.getTitle());
            songdetailsController.setArtist(object.getArtist());
            songdetailsController.setAlbum(object.getAlbum());
            songdetailsController.setGenre(object.getGenre());
            songdetailsController.setTime(object.getTimeencode());
        }
    }
    // Only work with list
    @FXML
    private void handleOpen(ActionEvent event) {

    }
    
    @FXML
    private void handleSave(ActionEvent event) {
    }

    @FXML
    private void handleSaveas(ActionEvent event) {
        FileChooser filechoos = new FileChooser();
        ExtensionFilter extFilter = new ExtensionFilter("EXTENSION XML", "*.xml");
        filechoos.getExtensionFilters().add(extFilter);

        File file = filechoos.showSaveDialog(mainmenu.getStage());
        configureFileChooser(filechoos);
        if (file != null) {
            //ext ok
            if (!file.getPath().endsWith(".xml")) {
                file = new File(file.getPath() + ".xml");
            }
            
        }
    }

    private void saveListSongXML(File f){
        try {
            JAXBContext context = JAXBContext.newInstance(Lista.class);
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            // Saving in local List
            Lista saviour = new Lista();
            saviour.setListSongs(this.list.getListaCancion());
            // envolving this object into file
            m.marshal(saviour, f);

//            // Save the file path into the registry for saving only not save as
//            Preferences p = Preferences.userNodeForPackage(ListController.class);
//            if (f != null) {
//                p.put("pathSong", f.getPath());
//
//                // Setting a title in the stage of this saved file
//                mainmenu.getStage().setTitle("Lister of songs  - " + f.getName());
//            } else {
//                p.remove("pathSong");
//                mainmenu.getStage().setTitle("Lister of songs ");
//            }
        } catch (Exception e) { // catches ANY exception
        	Alert alert = new Alert(Alert.AlertType.ERROR);
        	alert.setTitle("Error - Saviour");
        	alert.setHeaderText("Cant save");
        	alert.setContentText("Route data :\n" + f.getPath());
        	alert.showAndWait();
        }

    }
    
    @FXML
    private void handleOnblank(ActionEvent event) {
        list.getListaCancion().clear();
    }

    @FXML
    private void handleClose(ActionEvent event) {
        System.exit(0);
    }

    @FXML
    private void handdleMore(ActionEvent event) {
        
        FileChooser filechoos = new FileChooser();
        ExtensionFilter extFilter = new ExtensionFilter("All Songs", "*.*");
        filechoos.getExtensionFilters().add(extFilter);
            filechoos.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("WAV", "*.wav"),
                new FileChooser.ExtensionFilter("Mp3", "*.mp3")
            );
        // Para un unico archivo
//        File file = filechoos.showOpenDialog(mainmenu.getStage());
//        if(file!=null){
//            list.addToListSong(file);
//        }
        configureFileChooser(filechoos);
        List<File> listFiles =  filechoos.showOpenMultipleDialog(mainmenu.getStage());
        if (listFiles != null) {
            for (File file : listFiles) {
                list.addToListSong(file);
            }
        }
    }
    // set title and initial directory of this filechooser
    private void configureFileChooser(FileChooser fileChooser){
        fileChooser.setTitle("View diferent songs");
        fileChooser.setInitialDirectory(
            new File(System.getProperty("user.home"))
        ); 
    }
    @FXML
    private void handleLess(ActionEvent event) {
        int selectIndex = _songs.getSelectionModel().getSelectedIndex();
        if (selectIndex >= 0){
            //_songs.getItems().remove(selectIndex);
            list.getListaCancion().remove(selectIndex);
        }else{
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Unselect");
            alert.setHeaderText("Select item");
            alert.setContentText("Must to select one of the list item of the table. Please, pick one");
            alert.showAndWait();
        }
    }
}
