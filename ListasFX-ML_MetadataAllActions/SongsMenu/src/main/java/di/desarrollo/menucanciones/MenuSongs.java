/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package di.desarrollo.menucanciones;

import controller.ListController;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import utils.Window;

/**
 *
 * @author dirrospace
 */
public class MenuSongs extends Application {
    private Window w;
    private Stage stage;
    @Override
    public void start(Stage stage) {
        this.stage = stage;
//        w = new Window();
//        Parent root;
//        try {
//            root = (Parent) FXMLLoader.load(getClass().getResource("/fxml/list.fxml"));
//            
//            Scene scene = new Scene(root);
//            //scene.getStylesheets().add("/styles/main.css");
//
//            stage.setTitle("Mi lista de canciones");
//            stage.setScene(scene);
//            stage.show();
//            
//        } catch (IOException ex) {
//            Logger.getLogger(MenuSongs.class.getName()).log(Level.SEVERE, null, ex);
//        }
        // 2 forma
        FXMLLoader load = new FXMLLoader();
         try {  
            load.setLocation(getClass().getResource("/fxml/list.fxml")); 
            Parent padre =  load.load();
            ListController lc = load.getController();
            lc.setMainmenu(this);
            // Show the scene containing the root layout.
            Scene scene = new Scene(padre);
            stage.initStyle(StageStyle.UTILITY);
            stage.setScene(scene);
            stage.setTitle("Lister of songs ");
            stage.setResizable(true);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(MenuSongs.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    public Stage getStage() {
        return stage;
    }
    
}
