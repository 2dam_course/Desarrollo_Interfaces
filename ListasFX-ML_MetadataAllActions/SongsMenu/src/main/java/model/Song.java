/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.File;
import java.net.MalformedURLException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.MapChangeListener;
import javafx.scene.image.Image;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author dirrospace
 */

public class Song {
    private SimpleStringProperty type = new SimpleStringProperty(getClass().getName().toString());
    public StringProperty getTypeProperty(){ return type; }
    
    
    private  SimpleStringProperty title; // ó cancion
    private  SimpleStringProperty artist;
    private  SimpleStringProperty album;
    private  SimpleStringProperty genre;
    @XmlJavaTypeAdapter(utils.LocaldateConverter.class)
    private  ObjectProperty<LocalDate> timeencode;
    // Carga agil: la carga de un unico archivo en ram y no cargar todos de golpe(bajo rendim)
    private ObjectProperty<Image> cover;// ó portada
    // Media: son property pero permite tambien tratarlo con eventos
    private final SimpleObjectProperty<File> file; // ó archivo
    // ReadOnlyObjectWrapper: envoltorio del objeto de solo lectura de tipo MediaPlayer = musica
    private ReadOnlyObjectWrapper<MediaPlayer> player; // ó reproductor
    
   

    public Song(File file) {
        this.title = new SimpleStringProperty(this, "( title )");
        this.artist  = new SimpleStringProperty(this, "( artist )");
        this.album  = new SimpleStringProperty(this, "( album )");
        this.genre  = new SimpleStringProperty(this, "( genre )");;
        this.cover = new SimpleObjectProperty<>(this,"( cover )");
        this.genre  = new SimpleStringProperty(this, "( genre )");;
        this.timeencode = new SimpleObjectProperty<LocalDate>(this, "( timerEncoded )");
        
        this.file = new SimpleObjectProperty<>(this,"( file )");
        this.player = new ReadOnlyObjectWrapper<MediaPlayer>(this,"( player )");
        updateSong(file);
    }
    
    public void updateSong(File file){
        if(file != null){
            try {
                System.out.println(">>> * "+file.toString());
                final Media medio = new Media (file.toURI().toURL().toExternalForm());
                medio.getMetadata().addListener((MapChangeListener.Change<? extends String, ? extends Object> element) -> {
                    if (element.wasAdded())
                        chargeInRam(element.getKey(), element.getValueAdded());
                });
                // set get y property de todos los atributos que tenga
                player.setValue(new MediaPlayer(medio));
                player.set(new MediaPlayer(medio));
            } catch (MalformedURLException ex) {
                Logger.getLogger(Song.class.getName()).log(Level.SEVERE, null, ex);
                
            }
        }
    }
    
    public void chargeInRam(String key, Object value){
        System.out.println("Elemento de KEY "+key+" VALUE "+value.toString()+" que se cargara en ram");
        switch (key){
            case "title":
                setTitle(value.toString());
                break;
            case "artist":
                setArtist(value.toString());
                break;
            case "album":
                setAlbum(value.toString());
                break;
            case "genre":
                setGenre(value.toString());
                break;
            case "text-3":
                String fecha = value.toString();
                fecha = fecha.substring(fecha.lastIndexOf("=")+1,fecha.length());
                LocalDateTime ldt =LocalDateTime.parse(fecha, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                setTimeencode(ldt.toLocalDate());
                break;
            case "file":
                setFile((File)value);
            case "image": // !!!
                setCover((Image) value);
            default: break;
        }
    }

    public String getTitle() { return title.get(); }
    public StringProperty getTitleProperty(){ return title;}
    public void setTitle(String title) { this.title.set(title);}
    
    public String getArtist() { return artist.get(); }
    public StringProperty getArtistProperty() { return artist; }
    public void setArtist(String artist) {  this.artist.set(artist); }
     
    public String getAlbum() { return album.get(); }
    public StringProperty getAlbumProperty(){ return album;}
    public void setAlbum(String album) { this.album.set(album);}

    public String getGenre() { return genre.get(); }
    public StringProperty getGenreProperty(){ return genre;}
    public void setGenre(String genre) { this.genre.set(genre);}

    public LocalDate getTimeencode() { return timeencode.get(); }
    public ObjectProperty<LocalDate> getTimeencodeProperty() { return timeencode; }
    public void setTimeencode(LocalDate timeencode) { this.timeencode.set(timeencode);    }

    public Image getCover() { return cover.get(); }
    public ObjectProperty<Image> getCoverProperty(){ return cover;}
    public void setCover(Image cover) { 
        this.cover.set(cover);
    }

    public File getFile() { return file.get(); }
    public ObjectProperty<File> getFileProperty() {return file; }
    public void setFile(File file) {this.file.set(file); }

    public MediaPlayer getPlayer() { return player.get(); }
    public ReadOnlyObjectProperty<MediaPlayer> getPlayerProperty() {
        return player.getReadOnlyProperty(); }


    // --------------------------------------
    // otros atributos no usados               //||
    private  SimpleStringProperty year;        //||
    private  SimpleStringProperty track;       //||
    private  SimpleStringProperty albumartist; //||
    private  SimpleStringProperty composer;    //||
    // --------------------------------------    

}
