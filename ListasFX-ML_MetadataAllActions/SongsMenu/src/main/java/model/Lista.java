/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.File;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dirrospace
 */
@XmlRootElement(name = "songs")
public class Lista {
    @XmlElement(name = "song")
    private ObservableList<Song> listSongs ;

    public Lista() {
        this.listSongs =  FXCollections.observableArrayList();

    }

    public ObservableList<Song> getListaCancion() {
        return listSongs;
    }

    public void setListSongs(ObservableList<Song> listSongs) {
        this.listSongs = listSongs;
    }
    // Este file se crea externo. Resultante de coger la URL por medio de sistema
    public void addToListSong(File song){
        listSongs.add(new Song(song));
    }
    

}
